# privacy-policy
At widebisbd we respect the privacy of our users. This Privacy Policy explains how we collect, use, disclose, and safeguard your information when you visit our mobile communication application. Please read this Privacy Policy carefully. IF YOU DO NOT AGREE WITH THE TERMS OF THIS PRIVACY POLICY, PLEASE DO NOT ACCESS THE APPLICATION.

 

We reserve the right to make changes to this Privacy Policy at any time and for any reason. We will alert you about any changes by updating the “Last updated” date of this Privacy Policy. You are encouraged to periodically review this Privacy Policy to stay informed of updates. You will be deemed to have been made aware of, will be subject to, and will be deemed to have accepted the changes in any revised Privacy Policy by your continued use of the Application after the date such revised Privacy Policy is posted. This Privacy Policy does not apply to the third-party online/mobile store from which you install the Application, including any in-game virtual items, which may also collect and use data about you. We are not responsible for any of the data collected by any such third party.

 

COLLECTION OF YOUR INFORMATION

We may collect information about you in a variety of ways. The information we may collect via the Application depends on the content and materials you use and includes:

 

Personal Data:

We do not collect any persona data.

 

Social Networks Data: We does not collect any social networks data.

Geo-Location Information: We does not collect any geo-location information.

 

Mobile device access and mobile device data: Not applicable.

 

Push notification, third party data, and financial data: Not applicable.

 

USE OF YOUR INFORMATION

We do not use any personal data.

 

DISCLOSURE OF YOUR INFORMATION

We absolutely do not share any of your data with any third party. We use your data only internally to provide you customized and personalized app usage experience.

 

TRACKING TECHNOLOGIES

We does not use any tracking technologies.

 

INFORMATION SECURITY

We use administrative, technical, and physical security measures to help protect your personal information. While we have taken reasonable steps to secure the personal information you provide to us, please be aware that despite our efforts, no security measures are perfect or impenetrable, and no method of data transmission can be guaranteed against any interception or other type of misuse. Any information disclosed online is vulnerable to interception and misuse by unauthorized parties. Therefore, we cannot guarantee complete security if you provide personal information.

 

POLICY FOR CHILDREN

We do not knowingly solicit information from or market to children under the age of 13. If you become aware of any data, we have collected from children under age 13, please contact us using the contact information provided below.

 
Emails and Communications

If you no longer wish to receive correspondence, emails, or other communications from us, you may opt-out by contacting us using the contact information provided in the app or the one below.
